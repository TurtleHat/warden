# The Game Manager

The Game Manager (GM) is a special type of player with a different set of responsibilities. The GM...

- Narrates the actions of the non-primary characters (\[NPCs\]).
- Narrates and introduces random world events.
- Breaks ties on deciding when a die roll is needed.
- Manages which players act and when based on the situation.
- Provides mechanical and narrative consequences for dice rolls.
- Designs scenarios for the characters

## Scenes

A scene is a segment of related narrative. When the scene ends, the group, primarily lead by the GM, works to determine the next moment to focus on. This might mean skipping ahead mere minutes or weeks. Unless the rules of the game come into play like they do for travel, it's the group's job to decide what is interesting to them. Some conversations might be done in detail, others as summaries. Time in a village might be glossed over if the interactions aren't important to the PCs, or they could have many scenes with the various people who live there.

### Running Combat

It's just like normal scenes. Follow the fiction when prompting players to act. Use NPCs in response to PC failures. If the PCs are unable to act and the NPCs act, the players merely receive a consequence and have the opportunity to resist if they wish.
