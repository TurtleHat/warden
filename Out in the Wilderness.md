# Out in the Wilderness

## Encounters - Meeting People

When you meet someone, the lack of guaranteed safety out in the wilderness makes negotiating a shared understanding critical. A failed roll or tension bubbling over could result in flight, conflict, or other consequences. The PCs may choose to take any of the below actions steer the encounter.

### Noticing

When you enter a location, you can roll to keep your eyes out and notice anything important. On a miss, the GM chooses the range that any encounter begins at in a way favorable to the NPC. On a weak hit, nobody gets what they want. On a hit, the player chooses.

- Within sight
- Within shouting distance
- Within conversational distance
- Close enough to trade blows

### Establishing Peace

If you wish to actually avoid conflict with, learn from, aid, or get help from people you see on the road, you will need to create peace between the wardens and the other party.

Otherwise, you can act without regard to peace and the NPCs will act on their own, very likely with an abundance of caution.

#### Ratings and Rolls

Building enough trust to have [Peace] with other strangers while out in the wilderness is tracked as a rating. By taking the time to attempt peace, you are giving the initiative to the other party. They will be able to act immediately after the attempt.

A PC does a roll to start establishing trust. Peace is harder with NPCs something to hide or are criminals. A -1 or +1 result level may apply depending disposition of the NPCs. The GM uses context to decide. Success can guarantee peace. Pick a domain that matches how you start the interaction. Roleplay the action and roll to see the starting peace pool rating.

- **Critical Hit**: Peace rating starts at 3
- **Strong Hit**: Peace rating starts at 2
- **Weak Hit**: Peace rating starts at 1
- **Miss**: Peace rating starts at 0

You can add to your rating by doing each of the following things, each only once:

- Gift-giving
- Conceding an advantageous position or making yourself vulnerable
- Disarming your party
- Demonstrating common culture or interests
- Speaking peacefully and honestly

Roll dice equal to the peace rating.

- On a miss, the other party panics, flees, or attacks.
- On a weak hit, the GM chooses 1. On a hit, the GM chooses 2. On a strong hit, the GM chooses 3.
    - The party stays around
    - The party doesn't act aggressively
    - The party approaches you
    - The party lays down their arms

When trying to establish peace, roll dice equal to the rating. On a miss, the consequence is subtracting 1 from the rating. On a hit, add one to the rating. You can try to increase a trust pool several ways, using each method only once per pool:

- Speaking peacefully and honestly
- Disarming your party
- Gift-giving
- Giving ground
- Demonstrating common culture or interests

If you run out of ways to increase the pool and you have not hit your target, you may roll the pool one final time. Anything less than a strong hit counts as a breakdown in establishing peace.

### Approaching

Being noticed while approaching an NPC begins a peace roll. If the NPC gets nervous about an approach, you may need to roll to calm them down or they will act.

### Introductions

Once peace is established, NPC will not merely act out of fear, self-defense, or haste. If the characters are within shouting distance and have peace, you can make introductions.

Until [peace] is achieved, there is a very real chance that things could devolve to conflict of some sort.

#### Attitude

Every time you meet an NPC and make an introduction, record their name and an attitude they have for you. Attitude is a number ranging from -3 (hostile) to +3 (allied). The GM will adjust this number according to the situation.

### Peace

In order for a PC to break peace and start any sort of violent conflict, they must fail a Commons roll.

Breaking the peace or bypassing peace results in 1 stress. With witnesses, also take a [Reputation] Black Mark 1.

## Travel

To get from one place to another...

1.  The players choose a destination.
2.  The GM sets a clock size of 2 or more. 2 being very close and 10 being far. The GM may justify the value by describing the terrain, the danger, the party knowledge, or more.
3.  The players attempt to progress toward their destination
    1.  Up to three players choose tasks to contribute to. Tasks that are not done are treated as a miss.
2.  After resolving the rolls, the player arrive either at a [waypoint] or their destination.
	1.  When the travel clock fills the party has arrived once all the outstanding rolls are complete.
	2.  If the clock is not filled, the players arrive at a [waypoint]

Up to three actions can be taken by the group where players can contribute to travel.
If there are more than three PCs, the remaining PCs may choose to aid their allies. If there are fewer than 3 PCs, a single PC can do multiple actions. Each player's not aiding a roll only gets segment contributions from their first roll.

Each roll, regardless of the task, can contribute to the travel clock. Use the same roll result on the action tables below to see how your specific task progressed.

- **Critical Hit**: 2 segments added
- **Strong and Weak Hit**: 1 segments added
- **Miss**: No segments added

### Supplies

Roll the camp domain to see how supplies last.

- **Strong Hit**: Roll [field] to repair any applicable worn-down gear for up to three items.
- **Weak Hit**: Roll [field] to repair any applicable worn-down gear for one item.
- **Miss**: Each PC must do a wear roll one one of their items or take a wound.

### Gathering

Roll a domain reflecting how you are gathering to attempt to gain supplies. Such as using [forest] to hunt for food while traveling. A predatory or tracking [beast of burden] such as a [Sauroid] can contribute to these rolls.

- **Strong Hit**: Gain 2d worth of food, and supplies.
- **Weak Hit**: Gain 1d worth of food, and supplies.
- **Miss**: Each PC must test one of their food items or take a wound.

### Looking Out

A skittish animal like a horse can help you look out on the road. Roll the [forest] or [path] domain depending on the terrain.

- **Strong Hit**: As well as noticing potential danger, you see something noteworthy or beneficial while on the road such as shortcuts, unique supplies, other travelers, or possible [waypoints].
	- Shortcut: Add an extra segment to the map
	- A sight to behold: Take a moment to describe a beautiful natural scene. Everyone witnessing it clears one stress.
	- Friendly travelers: Discover people that may are likely to be friendly to you or provide aid.
	- Discover helpful supplies find something such as abandoned gear, potential crafting materials, or herbs.
- **Weak Hit**: You notice possible dangers in advance.
- **Miss**: You only notice inevitable landmarks and obvious things relating to your path but something else is out there.

### Making Camp

Roll [camp] to look out, cook food, etc.

- **Strong Hit**: Choose 2.
- **Weak Hit**: Choose 1.
- **Miss**: Any other party that finds you at your camp gets the initiative and chooses if and how to engage.

- Rest: Test medical supplies and removes a wound
- Lookout: Your campsite is well hidden or you can see any NPCs coming and prepare for their arrival
- Hearty Cooking: Ignore a failure on any one roll testing food supply.

### Trailwork

Roll [path] to maintain paths. [Beasts of burden] can always contribute to these rolls. These fill up [Trail clocks]

- **Strong Hit**: 2 segments
- **Weak Hit**: 1 segments
- **Miss**: You discover bad news about the state of the trails.

## Trails

### Trail Clocks

Clocks to improve a path start at 3 segments + the path quality. So a 1-quality path requires 4-segments to improve and a 4-tier clock takes 7.

## Waypoints

Waypoints are anything at all of note along a journey. This can be natural wonders, dangerous terrain, human constructions, temporary sites, or just a wide spot in the road. Waypoints...

- Allow for random events from the GM
- Give opportunities to roleplay scenes and challenges
- Present locations that the players may wish to map out to return to later

Waypoints have two major aspects to them:
- Geography: What is the waypoint like
- Event: What is happening

### Geography

There are times where the location is pre-determined by the narrative. Arrival at a final destination always has a pre-determined waypoint. If an NPC has already discussed what is up ahead or the player have already traveled on the route, the waypoint may be the same.

Otherwise, it is up to the GM to decide. When players roll a weak hit or a miss on their [looking out] roll, the GM will determine the location. There are tables for locations and events, both good and ill in the [Random Tables] section.

### Event

Events are a premise for interaction based on what's happening at the location. If the event involves NPCs, they are always doing something. There are specific [random tables] for what NPCs might be up to.

The GM uses discretion to decide how much to emphasize each event.

If it is not dangerous or a pressing matter, this might just mean making the players aware, using the event to establishing atmosphere as the PCs skim through a brief scene.

Other, dangerous or important events might challenge the players directly such that they cannot continue until they resolve the scene.

## Mapmaking

The all the players use a map to track the things that they discover on the fellwardens' journey. Create a visual map to track your adventure. But if you can't, use text to describe the connections.

1. When a players learn of a waypoint, document it on the map. Draw a circular node with the name of the location of a populate a hex with it. Place it roughly where you understand this waypoint to be relative to other things based off of what you know so far. Even if it is understood to be close, leave room for other thing in between.
2. Connect the waypoint with any others if you understand the path to be navigable. Draw a line connecting the two points. If you wish, embellish it with drawn details, text notes describing the path, or non-straight lines to match the PC's travels.
	1. Whey your players discover the quality of the trail that they are dealing with, add a quality tier and a clock or a fraction annotation for maintenance for when it is traveled.
4. During the travel, the group with discover other waypoints.
	1. Replace, modify or divert your waypoint connections to add noteworthy things that the group thinks is worth revisiting.
	2. For minor things such as possible camp sites or unexplored things that were noticed in the distance, little lines that stray off the path or free-floating nodes that suggest places to go. Annotate it in a way that helps you remember roughly the characters experienced without slowing down the group too much.
5. When you learn details about waypoints, put that in another location. Use a notebook, notecards, or a spreadsheet page for location tracking. Do not clutter your map with minute details.
