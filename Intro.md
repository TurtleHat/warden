# The Elevator Pitch

A joke: "The Dark Pastoral Fantasy Forged in the Dark OSR Storygame Walking Simulator Game Fellwarden"

In *Fellwarden*, you play people chosen by one town of several in a miserable and dangerous fantasy backwater travel the fells or other wilderness of the region, and solve local problems of safety or well-being.

Build trust with the locals, ensure safe paths between settlements, and explore the dangerous un-mapped places near your home.

## Vibes

- Roadwarden
- The Witcher
- Runequest
- Agon?

### Aesthetic

Simple, earthen colors. Basic embroidery and knots for decoration. Wet, cold, green and brown. Fog and worry.

## The Wardens

You and your fellows were chosen by your village to be the next wardens of your small, sparsely populated, dangerous region. You are charged with keeping the roads safe and walkable, and solving the problems of those along your route. Outsiders, newcomers and those with no prior obligations are often chosen first as a way of providing value and integration into a community.

You have no authority to order people around -- a warden's ability to help is based on their neutrality, shared interest in the well-being of the place they patrol, and goodwill.

You must do this for at least one year. At which point, the Wardens may return to their village to see if you will be selected again for the next season.

## The Region

The region that the itinerant wardens will patrol is their new homeland. It is dangerous, sparsely populated, and largely regarded as destitute. Most of the region is unmapped. At best, people have a few verbal directions and landmarks that let them travel outside of their villages.

There is likely no village of more than 100 people. No trade routes pass through here. Few people come here to trade, hardly anyone regards it as safe enough to justify leaving their homes.

During the first session, the group will create the region together. They will decide what the region is like, in what ways it is dangerous, and what merits it has that still make it a home for its occupants.

## Each Session

Each session is essentially an episodic view into the trials of a group of wardens on their journey.

Players will use rolls to summarize character travel and the state of their supplies. The wardens will arrive at various villages, camps and waypoints where they take on tasks to help the locals. When things are settled, they will continue on their way.

At first, nobody will know you. The players will have to build up their reputations with the locals as they go along.
