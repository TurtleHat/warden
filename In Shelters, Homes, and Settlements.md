# In Shelter

When a group of wardens arrive at a town, a guarded camp, or another form of reilable shelter, you must gain hospitality from someone in the town before you can take advantage of the place to rest.

## Hospitality

### What is Hospitality

When you arrive at someone's dwelling and ask for protection, your local laws of hospitality guarantee certain things: peace, shelter and food or water.

In order to gain these things from a host, the regional custom requires the guests to bring something in turn. The players choose what the custom requires:

- Kinship
- Honesty
- Prayers, blessings or rituals
- Gifts
- Stories
- Knowledge

Anyone, player or otherwise, who violates the laws of hospitality gains [Reputation] Inhospitable 2

### Gaining Hospitality

When PCs wish to gain a host and a place of safety, the must offer the traditional gift chose above to an NPC with shelter. If you have an inhospitable [reputation] then you must test it. Unless an inhospitable reputation, threats, or violence come into play, then you will be accepted and come under the laws of hospitality.

## Downtime

When in a shelter, each PC may take special [downtime activities]. Each PC gets two downtime activities.

If you want more activities, each PC who wishes to take another activities may impose on their host by testing a reputation or food to try to be self-sufficient.

Failure puts you out on the road to do your next leg of the journey. If it was a reputation toll, change the reputation for the worst.

### Clock Contributions

Some downtime activities use clocks to track progress. Anything in downtime that contributes to a clock gives the following values,

- **Critical Hit**: 5 segments
- **Strong Hit**: 3 segments
- **Weak Hit**: 2 segments
- **Miss**: 1 segment

### Spending and Gifting

Most everyone in the region has no use for money and so characters do not track it. Instead, players spend their reputations or trade equivalent quality goods. If you want an item of quality 2, you can either reduce your positive reputations by 2 or give up items that combine to a value of 2.

This does not mean that items have no value. Returning to town with valuables has utility: you can gift items and treasure in return for reputation.

For items, the trade offering items of equivalent of greater value simply happens. For reputation, you must roll your reputation to make sure that it can apply. On a hit, it applies. On a weak hit or a miss, you strain the reputation and it decreases.

## Downtime Activities

`TODO Idea:` Days and Nights activity cycle

### Recuperate

- **Dine Well**: Use a food item and roll camp to heal.
- **Avoid work**: Roll nothing at all. Choose one:
	- Remove a wound
	- Clear 3 stress and the GM negatively affects a reputation by one
- **Animal Care**: Use [field] to heal your animal.
	- On a hit, heal some 1 Roots.
	- On a weak hit or miss, add a complication tag to your animal's traits such as "stubborn," "skittish," or "limp."

### Trade and Favors

- Roll [commons]: Get news, rumors, and information.
- Get tools, supplies, or NPC expertise: Activate the rules above for spending and gifting.
	- [Commons] or reputation to trade or get expertise.
	- [Field] to make simple supplies

### Find Work

Roll [commons]
-  **On a Crit**:
-  **On a Hit**
-  **On a weak hit or miss**

### Work a project.

Create a clock after negotiating with the GM result.
- You might need specific tools, materials, NPC help or access to a work area.
- The GM and player must agree on the end result such as .
	- Add tags to things like your beasts
	- Create items with dice ratings
	- Change the problems or needs or an NPC or shelter

### Tell stories

- Reduce stress. Tell a story about a domain and roll it. Relieve stress equal to `TODO number` . Mark your domain storytelling box.
- Build reputation
- Build friendships: Roll commons or

### Craft or Repair

- Restore gear to its original strength

## Maybe?

- Mapping
	- easier travel later?
	- Increase reputation?

### Stress ideas

#### Argue about where to go next:

- Roll commons. Winner gets to choose where to go. The other participants relieve stress?

#### Gamblling and Games

How to make it work without money?

#### Tell domain stories?

Maybe only from trained domains?
