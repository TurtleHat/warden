# Your Warden

Choose the following things for your your primary character, known as a Warden.

- A Name
- Reason for setting out
- Goal

## Lifepath Chargen

`TODO` a bunch of questions about how to make a character instead of straight assignment.

No domain may be above 2 during character creation.

## Rules for a Warden

All wardens have a wounded and dying check box, 3 inventory slots, and a [reputation] of "New warden 1"

## Domains

Domains are used to determine the number of dice for a starting

Assign each Domain a value from this list: 0, 0, 1, 1, 2. Your roots are a 1.

- **Field**: Farming, animal care, harvesting, farm equpment,
- **Fells**: Difficult terrain, climbing, endurance and physical stregth, weather, celestial navitation
- **Path**: Travel, trail maintenance, handling long journies, travel ettiquitte
- **Camp**: Cooking, telling stories, keeping watch, managing supplies
- **Commons**: Getting jobs, barter, making friends, managing reputation
- **Wilds**: Hunting, gathering, pathfinding, dealing with wild animals, stalking
- **[Roots]**: see below

### Roots

Roots are always accompanied by a descriptor and a dice rating.

For primary characters, your [roots] are a place describing what character did before they became a fellwarden. These can be used in place of a any domain above when they are applicable.

- **Battlefield**: Fighting other humans, following orders, experiencing danger and death, marching
- **Book House**: Tradition, history, knowledge, writing, recordkeeping
- **City**: Trade, politics, ceremony, thievery
- **Distant Lands**: Rumors, languages, travellers, endurance, storytelling, navigation, asking questions, oral tradition
- **Home***: Food preservaton and preparation, building maintenance, cleaning, managing, serving, hosting, basic medical care
- **Landless**: Scavanging, making camp, shelter-building, weather-wise, water-finding
- **Tower**: Ceremony, potions, religion, sage advice, studying
- **Tunnel**: Mining, underground dangers, finding gems, spelunking
- **Waters**: sailing, fishing, boating, ropes
- **Workshop**: complex crafting, and repairs

#### Examples



### Practice

Each domain has 3 boxes next to it.

Boxes:
- Mark the first box when the PC uses the domain and gets a hit
- Mark the second box when the PC uses the domain and gets a miss
- Mark the third box when the PC relieves stress by telling a story of a domain

Once all three are marked, a PC may use a downtime activity `TODO set this in stone` to increase a domain rating.

## Reputation

A character has room for six different reputations.

Reputations track what people say about the character. This is the sort of thing that characters become known for. If you have a reputation, it means that there are things NPCs think of you that they may share with others.

Reputations are often, but not always true, but they are almost always created as a result of PC action. For a PC to remove or change an action, they must act. Without acting, reputations cannot change.

Reputation can be used to modify how many dice you roll and set initial reactions from NPCs.

Each reputation has a rank from 0 to 4. Rank 4 reputations are the strongest and most well known.

## Stress

Each Warden has 6 stress boxes. Stress represents the PC's ability to draw on luck, endure tough situations, and pull from a reserve of strength or willpower.

### Push Yourself

When you are not aided by items or an ally, you can push yourself by spending two stress to get +1d to your roll.

### Resist Consequences

When you get a miss or a weak hit on a die roll, the player can resist the consequences. Roll a domain or piece of gear that you are using to avoied the conseqence and narrate how it happens. Spend stress equal to the lowest die roll.

When you resist a consequence using a domain, mark the trained box for that domain.

### Running out of Stress

When a PC marks their last stress box:

- The PC becomes incapacitated until they can rest at camp.
- The PC gets a new rank 1 reputation relating to their overwhelming stress such as "Burned out," "Jaded," or "Weak knees"

## Items and Inventory

Inventories start with two items, chosen from the following list:

- Spear 2/2
- Bow & arrows 2/2
- Walking staff 2/2
- Cloth armor 2/2
- Travel Rations 3/3

Most items that your character owns are not covered

## Your Beast of Burden

Part mount, part pack animal. Your only guaranteed companion on the road. Your beast will have a species, a name, roots, and two inventory slots. Its dice rating is 2.

### 🐎Horse

- Three inventory slots
- Roots 2
  - Ride, haul, gallop, skittish

### 🦎Sauroid

- Two inventory slots
- Roots 2
  - Ride, bite, haul, swim

### 🐐Goat

- Add two inventory slots.
- Roots 2
  - Ride, haul, climb, eat

### 🐃Buffalo

- Four inventory slots.
- Roots 2
  - Ride, heavy haul, endure, plod

## Advancement

Mark an XP when:

- You finish helping a local
- You create, reinforce or counter one of your reputations
	- If you reinforce a reputation, increase its rank by 1. When a reputation would go above 4, rephrase it to something stronger and restart it at 0.
	- If you countered a reputation, reduce it by 1. When a reputation would fall below 0, remove it.
- You travel through the wilderness to arrive at your destination.

### When you fill the track

Choose one of the following and clear the XP track:

- [ ] **Expertise**: Increase the rank of one of your domains with a marked "trained" box by one
- [ ] As above
- [ ] As above
- [ ] **Wanderer**: Gain an additional inventory slot on you and your beast of burden.
- [ ] **Quality**: Increase your character quality by 1. You start at tier 0. Reduce the amount of dice lost from any opponent roots rating by your quailtu.
- [ ] **Attunement**: If you have roots in the Tower, add one more possible spell effect
- [ ] **Sentinel**: You you keep a look out, people cannot get the drop on you.


## Magic

Magical miasma floats through the land like an invisible fog. Sometimes it rests on living things like a dew and changes them in strange ways. For most things, this does harm: it kills, corrupts, and tortures. A small minority possess enough resistance to the magical corruption that they can hold on just long enough to use the miasma's altering nature to their advantage.

**Only PCs who have [roots] from the [Tower] have this power**. [Roots] in the [Tower] can represent any training in ritual magic or spiritualism from any philosophy. This includes religious traditions, scholarly attempts to understand magic, and anything in between.

It takes time to concentrate enough of the magic to make a targeted change in the world. This is only done in the form of a ritual. Rituals can be done out in the wilderness when [making camp] and as a downtime action. Add the [Conduct a Ritual] action as an option to the list of the [Making Camp] travel action and as a downtime action when resting at a shelter.

For NPCs, this ability is extremely rare. It is covered in the [NPCs] chapter.

### Conduct a Ritual

Roll [Tower], describe your ritual, and what your desired result is.

- **Critical Hit**: Choose 4
- **Strong Hit**: Choose 3
- **Weak Hit**: Choose 2
- **Miss**: Choose 1

Options:
- You extract useful information about your current conundrum and possible immediate next steps
- Dispel any magical energy from this area
- You concentrate magic into an object for one later use, which can be used as an item but never repaired.
- You remove the dying condition from a character and give another character a wound.
- Double the effect of an above choice
- You avoid gaining the [reputation] corrupted soul 1
- You prevent the magic from changing the world nearby in unforeseen ways

Any character with [Roots] in the [Tower] starts with three magical concentrations. Additional ones can be unlocked withe the [project] [downtime activity]. The PC must either witness the magic, find an item that contains or documents the magic, or discover something that represents the magic in order to do the project. The clock size for this project will be the number of existing effects + 1. If a character has three spells, their fourth spell will be a four-segment clock.

Any time you use an item to inflict wounds and the target is not resistant to the method of attack (like trying to drown a monster that does not breathe), it deals 2 wounds.

#### Magics

A new player should choose three of these possible magics that they can concentrate into items. The list is not intended to be exhaustive. Groups who are interested should customize and expand the list to fit their game.

- Summon the wind
- Conjure or manipulate fire
- Warp or weaken the earth
- Move, repel, or concentrate water
- Banish the Darkness
- Wreath one or more weapons in an element: set them alight, or freeze coat in painful frost, etc.
- Bind a foe with roots and vines
- Switch places with someone up to 20 yards away
- Make someone’s hair grow rapidly and ensnare them
- Create a flash that might distract or blind a foe
- Rot wood in a matter of seconds
- Create terrifying but harmless shadows
- Briefly appear to be three times your size
