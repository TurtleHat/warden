# The Region

You region is isolated by its geography. Choose how:

- Mostly surrounded by mountains
- On a peninsula
- Hard to navigate swamps
- Very far away
- Island in dangerous waters

Your region always has the danger of bandits.

- Choose 2 dangers of the region:
  - Enveloping beetle hives
  - Demons
  - Undead
  - Ghosts
  - Deadly plant life
  - Fungal Goblins
- Choose 1 creeping threat:
  - Spreading sickness
  - Oppressive fog
  - Dying fields
  - Endless rain
  - Endless snow
  - Disproportionate darkness
  - A curse
- Choose why selfish outsiders might care care:
  - Possible trading shortcuts
  - Minerals in the hills (gold? Silver?)
  - Rare plants
  - Rare creatures
  - Rich farm or ranch land

Create 6 villages, which exist in an approximate ring:

## Villages

Each village has...

- A name
- A dire need
- Size: Approximation how of many tens of people live here. 0-10.
- Up to its size in abundances
