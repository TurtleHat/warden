# Oracles, Random Tables, and Events

All random tables here are designed to be used with two distinct six-sided dice. The first die is used to look up a second table, where you get the final result. Either roll two dice, such as a light and a dark die, or roll one die first and then the other.

## Favors, Tasks and Chores

### 1. Danger - nature

1. Dangerous beast is causing trouble
2. Dangerous monster causing trouble
3. Crops are withering and dying - Something is wrong with the water
4. A mudslide or similar has caused a path to be blocked
5.

### 2. People Problems

1. Two parties are having an argument. Maybe you can be neutral jugdes?
2. Outcasts

## Places Out in the Wilderness

### 1. Structures
1. Cabin
2. Cave Shelter
3. Stone gatehouse or tower
4. Tent
5. Stick tent thing
6. Altar or small chapel

### 2. Animal Things

1. Burrow
2. Nest
3. Den

### Temporary Things

1. Wagon



## Signs That Things Have Gone Wrong

1. An additional Moon
2. Stangely colored Fog
3. A weird hum
4. Stones, trees, etc. are bleeding
