# Words, Ideas, and Other Things

## Mail delivery mechanic?

"Vagrant"

## Cool places

- A gate
- A weird clearing
- A religious site
- Unpleasant insects

## Details on Lichen Humanoids / Lichenthropes / Fungal Goblins


Me: I did add something to my setting as an optional threat to the world which are "fungal goblins" which I think are gonna be a non-senteint aggressive fungus that basically behaves in a preprogrammed way and happens to move bipedally, but it's entirely unthinking and not unlike an ant hive
Just weird and scary since they walk on two foots so it's good for spooky stories to keep the children home at night

Wr: An ever-growing nonsentient fungus without mind or center which behaves in a preprogrammed way and only wants to grow and grow forever in spite of any damage and disruption that causes is a pretty resonant threat.


## Why are chimeras monsters?

So why are Chimeras on the list? Probably because they're, I don't know, weird combinations of other animals in horrible pain because of their strange form


## Spell Ideas

Ideas from Harrigan
- You cloak yourself and [ tied to success level ] others in invisibility, which lasts as long as you can hold your breath.
- Manipulate something delicate, like a key in a lock, up to [ tied to success level feet or meters away ]
- Set one or more weapons alight
- Bind a foe with roots and vines
- Freeze metal and cover it in painful frost
- Switch places with someone up to X distance away
- Make someone’s hair growth and ensnare them
- Create a flash that might distract or blind a foe
(Switched to quick fire)
- Cause landslide
- Rot wood
- Create terrifying (but harmless) shadows
-Loom — briefly appear to be 3x your size
- Time skip — go first in combat, increased defenses vs first for striking you


## How does the bartering system work and why

I haven't written it out yet, but it's basically a mutual understanding that money has no use to anyone, most people know everyone, and so it's kind of a system where everyone understands that favors are always cashed in back and forth forever, but doing too much of it hits your reputation and you'd be stuck to direct goods.
Gary G — Today at 1:54 PM
Gotcha. So the breadmaker makes bread knowing they can get other resources as recompense, assuming the breadmaker doesn't behave poorly.
Turtle Hat (he/him) — Today at 1:55 PM
Yeah. And the scale of my game is super small. There will probably be... one or two places that the players visit where there are at or around 100 people at most. Groups of 10-30 are much more the norm
Gary G — Today at 1:55 PM
That makes it much more manageable. Micro instead of macro.
Turtle Hat (he/him) — Today at 1:57 PM
And so when you impose, because the scale is so small, even if you maybe get away with not having to pay back what you borrowed in another community, it be that the next people from your home who come through would pay the price instead.
