# NPCs: Humans, Animals, and Monsters

There are three kinds of life that the party will encounter within their region that the GM is responsible for controlling: Humans, animals, and monsters.

**[Humans]** are the only life in this area of the world known to be sapient. They have additional rules for tracking attitudes with the party, and their ways of approaching the world.

**[Animals]** are simple creatures who have no inherent hostility to humans and no special powers. Animals can usually be understood by their nature. Hostility might come from fear, hunger, or territorial needs.

This includes many conventional beasts such from horses, ducks, and wolves. This also includes also other fantastic but natural beasts, such as the tame lizard-like riding sauroids, giant spiders, and sprawling predatory beetle hives, or whatever else might belong in your setting.

**[Monsters]** are any sort of creature with hostile or incomprehensible intent, potentially exotic powers, and usually no sapience. When sapient, a monster's soul is bent into a tortured mockery of its true self, like as a ghost, where being freed from its shell would return it to peace.

This includes the undead, lichen goblins, ghosts, chimeras, or whatever belongs in your region.

## Humans

Human NPCs have three things that are tracked as numbers:

- Roots: Who they are as a person. A primay defining feature such an identity, job, expertise or role
- Goal: What they want to accomplish
- Strategy: How they accomplish goals
- Attitude: NPCs track attitudes toward the PCs they meet.

When NPCs, creatures, or Monsters roll do do something against their roots, they roll half their roots rounded down.

For all other sentient life, \[Roots\] are how they were raised and their life up to this point: what they do for a living, or how they see themselves.

All recorded NPCs, even if it doesn't come up for the players, are named when recorded.

There is guidance in the form of a small list of things that are covered by their roots. Sometimes, these are verbs, sometimes they are nouns, but they are the things that their \[Roots\] can help with.

For an an animal or monster, their \[Roots\] are merely their species.

## Injuries

Injuries to NPCs are tracked as reducing their \[Roots\]. A human with a Roots of 3 may be injured once to 2/3. If they go to 0, they are dead or drying. Reduced roots values will `TODO`

### Attitude

- -3: Hostile. NPCs who are hostile to you will go out of the way to inconvenience or harm you, even at risk to themselves.
- -2: Untrusting. `TODO`
- -1 Disliked. An NPC who dislikes you might not help you and might speak ill of you, but they are unlikely to take action on their own to cause you trouble.
- 0 Neutral. This NPC has no particular opinions about you.
- +1 Cordial. `TODO`
- +2 Friendly. `TODO`
- +3 Allied. `TODO`

## Examples

- Ingrid of Bridletap (she/her)
    - Roots: Innkeep 2
        - Serve food, keep the peace, gossip
    - Goal:
    - Attitude:
    - Strategy:

- Yves the Stone Eye (she/her)
    - Roots: Hunter 3
        - Track prey, prepare meat, foraging

- Obal of Brownhill (he/him)
    - Roots: Vagrant 2
        - Steal, wandering, borrowing

- Pack Horse
    - Roots 2
        - Carry, haul, flee, herd

- Warg
    - Roots 4
        - Stalk, stay with the pack, terrifying howl, gnashing teeth, surprising intelligence, cruel streak

## Magic
Maybe two or three NPCs in the entire region have this ability. It's rare. It's hard to control.

`TODO` rules for NPC magic

An NPC with magical ability has a second set or roots, which name the style of magician that they are.

For an NPC that can go magic to be killed, first their magical roots must be burned out. Then they must be killed as normal. When the magical roots are removed, their magic fails them, turns inwards, and changes them irrevocably.

Otherwise, Magician NPCs act as anything else does. When the group looks to the GM to see what happens and a magician acts, or when a consequence from a failed roll involves magic, the GM narrates what happen. Magic is powerful, so it might be one of the very rare situations when more than one wound is given at once.

Magic in Fellwarden is slow and ponderous. Instant magical effects always come from past spells being stored into objects of magical power. Keep this in mind when creating NPCs and narrating magic.

### For example:
Necromancer 3. Control bodies, warp and rot flesh.
