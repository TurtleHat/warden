# Rolling Dice

## When to roll

Roll dice to find out how a situation is resolved when...

- The outcome is uncertain
- The outcome is important to the story
- The outcome can be changed by either
    - Character skill
    - Character action
    - Character reputation
- A item gets used

## Choosing what to roll

Players typically decide what the subject of their roll is, which determines how many dice they roll. When a PC takes an action, they choose a domain that matches the narration of their action. So a character in a field who is trying to trave with a farmer will roll [commons] because [commons] is the location where you gaind the skills to trade.

A player rolls a number of six-sided dice equal to their domain rating minus any applicable opposing NPC roots.

In some rare situations, an outcome is uncertain and it is primarily influenced by something besides a playes. If that is an NPC, the interested person rolls the NPC's current rools so see what happens.

## Interpreting Rolls

Players do almost all the rolling. If the number of dice would be a zero or lower, written as 0d, roll two dice and consider only the lowest one. Look at the dice to reach a conclusion about what happens:

- **Multiple sixes**: Critical Hit. You get more than you could expect.
- **One six:** Strong Hit. You succeed at your goal or get a positive outcome.
- **Highest result is a four or five**: Weak Hit You get what you want, but with complications chosen by the GM, which might include some of the consequences for failure listed below:
- **All dice threes or lower**: Miss. Only complications chosen by the GM, chosen from this list:
    - Cost (such as time, resources, or reputation)
    - Harm
    - Delay
    - Spillover (secondary consequences)
    - Reduces effectiveness
    - Revelation of bad news
    - Confusion
    - Waste

## Wagering Dice

For rolls where a primary character it acting against an NPC, Players can choose to withhold dice from their roll and the GM can choose to reduce the roots of an NPC. Keep the number of dice withheld secret until both the player and the GM have decided. No wager can reduce the number of dice below 0d.

The person who wagered the most dice may change the result level by one in a direction that favor them. For example, a player turning a weak hit into a strong hit or an NPC turning a weak hit into a miss.

## Aid

You can get aid either from your gear, your mount, or a person. When you do this, you put your source of aid at risk. If that aid is from a person, they are at risk of the same consequences as you. If you use gear, it could be lost or damaged.

## Combat

Combat, unlike many things characters may do, has no standard domains that cover it. Unless your character has roots in a battlefield, you roll as though your skill is zero. You can increase it with aid.

A miss and a weak hit for a combat roll always results in the harm consequence.

## Harm

When a character, animal, or monster is harmed, is it marked as wounded unless armor can be used to avoid harm. If a wounded character is harmed, they are \[Dying\].

### Dying

Dying characters need tended to immediately with a successful applicable roll or they die at the next fictionally appropriate moment.

## Using items

When you use an item, roll its wear rating. A miss or a hit reduce its rating by one. An item is destroyed rendered unusable when it would be reduced to a rating below zero.

Items take up one inventory slot each, regardless of how large the item is physically.

Simple gear can be restored to its original rating by doing field domain rolls for simple gear and workshop domain rolls for more complex gear.

Items do the following things:
- Give permission: You can't attack at a distance without a bow or do magic without a magic item.
- Protect from consequences: You can prevent a wound with armor or avoid starving with food
- Avoid rolling: Rope can make it safe climb down a wall

## Leveraging Reputation

If any player thinks a certain reputation may affect an NPC's attitude or a roll, choose a reputation and make a \[reputation\] roll before the introduction. This can only happen once for any give attitude or scene.

When you do a reputation roll, roll according to the rank of your reputation in question. Roll -1 die for each of the questions below where the answer is no.

- Are you near to civilization?
- Are people likely to talk about this reputation?
- Is this reputation applicable to the situation?

On a hit, the other party has heard something of your reputation. On a critical hit, they know details. On a weak hit, they only know the most vague aspects. This will affect what the NPC thinks of the player from now on. You can get -1 or +1 die on the NPC attitude from a hit, depending on how the NPC would feel about the reputation.
